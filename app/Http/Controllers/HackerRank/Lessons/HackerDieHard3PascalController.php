<?php

namespace App\Http\Controllers\HackerRank\Lessons;

use App\Http\Controllers\Controller;

class HackerDieHard3PascalController extends Controller
{
    public function diehard3()
    {
        function solve($a, $b, $c) {
            if (
                $a % $c === 0 ||
                $b % $c === 0 ||
                ($a + $b) % $c === 0 ||
                abs($a - $b) % $c === 0
            ) {
                echo "YES";
            } else {
                echo "NO";
            }
        }

        $res = solve(5, 3, 4);
        dump($res);
    }
}
