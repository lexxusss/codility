<?php

namespace App\Http\Controllers\HackerRank\Lessons;

use App\Http\Controllers\Controller;

class HackerRankController1 extends Controller
{
    public function matrix_tracing()
    {
        function solves($n, $m) {
            $max = pow(10, 9) + 7;
            if ($n < 2 || $m < 2) {
                return 1;
            }

            return solves($n, $m-1) % $max + solves($n-1, $m) % $max;
        }
        function solve($n, $m) {
            $max = pow(10, 9) + 7;
            $rows = [array_fill(0, $m, 1)];
            for ($i = 1; $i < $n; $i++) {
                for ($j = 0; $j < $m; $j++) {
                    $rows[$i][$j] = (($rows[$i][$j-1] ?? 0) + $rows[$i-1][$j]) % $max;
                }
            }

            return $rows[$n-1][$m-1];
        }

        function power(int $base, int $power): int {
            if ($power === 0) {
                return 1;
            }
            if ($power === 1) {
                return $base;
            }
            $max = pow(10, 9) + 7;
            $temp = power($base, $power / 2);

            if ($power % 2) {
                return ($base * ($temp % $max) * ($temp % $max)) % $max;
            }

            return (($temp % $max) * ($temp % $max)) % $max;
        }

        function inversePower($base): int
        {
            return power($base, pow(10, 9) + 5);
        }

//        // $a = 5, $p = 3; $a % $p != 0;
//        ($a^($p-1) - 1) % $p == 0;
//        $a^($p-1) % $p == 1 % $p;
//        $a^($p-2) % $p == $a^(-1) % $p
//        $a^($p-1) % $p == 1; // now divide on p both sides
//        ($a^($p-1) % $p)/$p == 1/$p;
//        $p == 1/$p;
//        $p = 1;-1;  // let's use "1" and formula: "$a^($p-1) % $p == 1;"
//        $a^($p-1) = 1;
//        $p - 1 = 0;
//
//        ($a^($p-1) - 1) / $p == $C;
//        -------------------------
//        $a^($p-1) - 1 == $p*$C;
//        $a^($p-1) == $p*$C+1;
//        -----------------------
//        $a^2 == 3*$C + 1;
//
//        25 = 3 * $C + 1;
//        24 = 3 * $C;
//        $C = 24 / 3 = 8;
//
//        $d = $p % $x == 0;
//        5 == log(3*8+1, 2);
//        dd(262144 / 7);
//        dd(5 % 3);
        dd(power(8,3));


        /**
            1 1 1  1  1   1
            1 2 3  4  5   6
            1 3 6  10 15  21
            1 4 10 20 35  56
            1 5 15 35 70  126
            1 6 21 56 126 252
         */
        $inputs = [];
        $inputs[] = [2,1]; // 1
        $inputs[] = [1,2]; // 1
        $inputs[] = [2,2]; // 2
        $inputs[] = [2,3]; // 3
        $inputs[] = [3,2]; // 3
        $inputs[] = [3,3]; // 6
        $inputs[] = [3,4]; // 10
        $inputs[] = [4,2]; // 4
        $inputs[] = [2,4]; // 4
        $inputs[] = [4,4]; // 20

        $inputs = array_map(function ($item) {
            return explode(' ', rtrim($item));
        }, file(storage_path('input_matrix_tracing_case_1_test.txt')));
//
//        $inputs = [[3,4]];

        foreach ($inputs as $input) {
            list($n, $m) = $input;
            $res = solves($n, $m);
            dump($res);
        }
    }
}
