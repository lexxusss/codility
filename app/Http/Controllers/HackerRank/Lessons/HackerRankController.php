<?php

namespace App\Http\Controllers\HackerRank\Lessons;

use App\Http\Controllers\Controller;

class HackerRankController extends Controller
{
    public function camel_case()
    {
        function camelcase($s) {
            $number = 1;
            $len = strlen($s);
            for ($i = 0; $i < $len; $i++) {
                if (ctype_upper($s[$i])) {
                    $number++;
                }
            }

            return $number;
        }

//        $candles = [3,2,1,3];
        $candles = [4,4,1,3];

        $res = birthdayCakeCandles($candles);
        dd($res);
    }

    public function birthday_cake_candles()
    {
        function birthdayCakeCandles($candles) {
            $byKeys = [];
            foreach ($candles as $candle) {
                $byKeys[$candle] = $byKeys[$candle] ?? 0;
                $byKeys[$candle]++;
            }

            krsort($byKeys);

            return array_shift($byKeys);
        }

//        $candles = [3,2,1,3];
        $candles = [4,4,1,3];

        $res = birthdayCakeCandles($candles);
        dd($res);
    }

    public function even_odd_query()
    {
        function solve($arr, $queries) {
            $output = [];
            foreach ($queries as $query) {
                if (isOdd($query[0], $query[1], $arr)) {
                    $word = 'Odd';
                } else {
                    $word = 'Even';
                }
                $output[] = $word;
            }

            return $output;
        }

        function isOdd($x, $y, $arr) {
            if ($arr[$x-1] % 2) {
                return true;
            }

            if ($x != $y && $arr[$x] == 0) {
                return true;
            }

            return false;
        }

        $arr = [3,2,0,7];
        $queries = [
            [2,3],
            [2,2],
        ];
//        $arr = [3,2,7];
//        $queries = [
//            [1,2],
//            [2,3],
//        ];

        $res = solve($arr, $queries);
        echo implode("<br/>", $res);
    }
}
