<?php

namespace App\Http\Controllers\HackerRank\Interviews;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InterviewsController extends Controller
{
    /**
     * https://leetcode.com/discuss/interview-question/819577/throttling-gateway-hackerrank
     *
     * Algo Qn1: Throtting Gateway
     * Non-critical requests for a transaction system are routed through a throttling gateway
     * to ensure that the network is not choke by non-essential requests.
     *
     * The gateway has following limits:

     * The number of transactions in any given second cannot exceed 3
     * The number of transactions in any given 10 second period cannot exceed 20.
     * A ten-second period includes all requests arriving from time max(1, T-9) to T(inclusive of both) for any valid time T.
     * The number of transactions in any given 60 second period cannot exceed 60.
     * Similar to above, the 1 minute is from max(1,T-59) to T.

     * Any request that exceeds any of the above limits will be dropped by this gateway.
     * Given the times at which these requests arrive in ascending order, find how many requests will be dropped.

     * NOTE: Even if a request is dropped it will still be considered in future calculations.
     * Although, a request is to be dropped due to multiple violations, it is still counted only once.

     * Examples:
     * Sample Case0:

     * Sample Input:
     * requestTime = [1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,11,11,11,11]

     * Sample Output:
     * 7

     * Request 1: Not Dropped
     * Request 1: Not Dropped
     * Request 1: Not Dropped
     * Request 1: Dropped. Atmost three requests are allowed in one second.
     * No requests will be dropped till 6 as all comes to an allowed rate of 3 requests per second
     * and the 10 second clause is also not violated.
     * Request 7: Not Dropped. The total number of requests has reached 20 now/
     * Request 7: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Request 7: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Request 7: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Request 11: Not Dropped. The 10 second window has not become 2 to 11.
     * Hence total number of requests in this window is 20 now.
     * Request 11: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Request 11: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Request 11: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Request 11: Dropped. Atmost 20 requests are allowed in ten seconds.
     * Also at most 3 requests are allowed per second.

     * Sample Case1:

     * Sample Input:
     * requestTime = [1,1,1,1,2]

     * Sample Output:
     * 1

     * Sample Case2:

     * Sample Input:
     * requestTime = [1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7]

     * Sample Output:
     * 2

 */
    public function throttling_gateway(): void
    {
        function solution($data) {
            $broken = 0;
            $brokens = [];
            $grouped = getGrouped($data);
            $sums = getSums($grouped);
            dump($sums);

            $gateways = [
                '1minGateway' => ['sec' => 60, 'amount' => 60],
                '10secGateway' => ['sec' => 10, 'amount' => 20],
                '1secGateway' => ['sec' => 1, 'amount' => 3],
            ];

            foreach ($sums as $sec => $amount) {
                $brokenByGateway = null;
                foreach ($gateways as $gatewayName => $gateway) {
                    if ($brokenByGateway && $brokenByGateway !== $gatewayName) {
                        break;
                    }
                    if (isGatewayBroken($brokens, $sec, $amount, $sums, $gateway['sec'], $gateway['amount'])) {
                        $broken++;
                        $brokenByGateway = $gatewayName;
                    }
                }
            }

            return $brokens;
        }

        function isGatewayBroken(&$brokens, $sec, $amount, $sums, $thresholdSec, $thresholdAmount): bool {
            $reqAmount = $amount - ($sums[$sec-$thresholdSec] ?? 0);

            $exceededRequestsAmount = $reqAmount - $thresholdAmount;
            $broken = $exceededRequestsAmount > 0;
            if ($broken) {
                $brokens[$sec][] = [
                    'exceededRequestsAmount' => $exceededRequestsAmount,
                    'gateway' => ['sec' => $thresholdSec, 'amount' => $thresholdAmount],
                ];
            }

            return $broken;
        }

        function getSums(array $grouped): array {
            $previous = 0;
            $i = 1;
            $sums = [];
            foreach ($grouped as $sec => $amount) {
                while ($sec !== $i) {
                    $sums[$i] = $previous;
                    $i++;
                }
                $sums[$sec] = $sums[$sec] ?? 0;
                $sums[$sec] = $amount + $previous;
                $previous = $sums[$sec];
                $i++;
            }

            return $sums;
        }

        function getGrouped(array $data): array {
            $group = [];
            foreach ($data as $sec) {
                $group[$sec] = $group[$sec] ?? 0;
                $group[$sec]++;
            }

            return $group;
        }

        $data = [1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,11,11,11,11]; // 7
//        $data = [1,1,1,1,2]; // 1
//        $data = [1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7]; // 2

        $res = solution($data);
        dd($res);
    }
}
